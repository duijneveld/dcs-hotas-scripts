# DCS WORLD F-18 HOTAS  Script

## Getting Started
1. Download the repository
2. Edit `config.ttm`
3. Open `f18.tmc` using 'TARGET Sript Editor' and hit run.

## Current Configuration
The following is a list of all the controls that have been implemented in the script.

* Engines OFF/IDLE
* COMM1/COMM2
* Radar Elevation
* Radar Target Designation
* Chaff/Flare Dispensing
* Exterior Lights Switch
* Cage/Uncage
* A/A Weapon Selection
    * Sparrow
    * Sidewinder
    * AMRAAM
    * Gun
* Trim
* Undesignate/Nosewheel Steering
* Paddle Switch - Autopilot/Nosewheel Steering Disengage
* Sensor Select Switch
* Master Mode Buttons
* Throttle Designator Controller
* Flaps
* A/G Weapon Release Button
* Gun/Missile Trigger
* Fuel probe Extend/Retract
* Master ARM ARM/SAFE
* Throttle Finger Lifts

## Key Mapping
Key mappings are defined in the `f18-keys.ttm` file to ease maintenance. Note that at the moment some of the keybindings are defined in `f18.tmc`, but that will be changed in future updates.

## Configuration
The script is configured using the `config.ttm` file.

Note that some keyboard bindings are not configured by default in DCS World and need to be added for the script to work properly.m Please see `keyboard.html` for the keyboard configuration.

Available options are:
* **ThrustmasterRudderPedals**  
If set to 1, Thrustmaster's Rudder Pedals will be included. Set to 0 if pedals are not present.
* **vrMode**  
If set to 1, the trim switch will control the trim duh!. Set it to 0 and the Trim switch will move the view around.

## Diagram
Here is a diagram of the current config.
![HOTAS Diagram](HOTAS.png)